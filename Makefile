postgres:
	docker run --name postgresBank -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -d postgres

createdb:
	docker exec -it postgresBank createdb --username=postgres --owner=postgres postgresBank

dropdb:
	docker exec -it postgresBank dropdb postgresBank -U postgres

startdb:
	docker start postgresBank

migrateup:
	migrate -path db/migration -database "postgresql://postgres:password@localhost:5432/postgresBank?sslmode=disable" -verbose up

migratedown:
	migrate -path db/migration -database "postgresql://postgres:password@localhost:5432/postgresBank?sslmode=disable" -verbose down

sqlc:
	sqlc generate

test:
	go test -v -cover ./...

server:
	go run main.go

mock:
	mockgen -package mockdb -destination db/mock/store.go gitlab.com/josephhlwang/gobank/db/sqlc Store

.PHONY: postgres createdb dropdb migrateup migratedown sqlc test startdb server mock