package db

import (
	"database/sql"
	"log"
	"os"
	"testing"

	_ "github.com/lib/pq"
	"gitlab.com/josephhlwang/gobank/util"
)


var testQueries *Queries
var testDB *sql.DB

func TestMain(m *testing.M){
	config, err := util.LoadConfig("../..")
	if err != nil{
		log.Fatal("Cannot load config.", err)
	}

	value := os.Getenv("CI")
	uri := config.DBSource
	if value != "" {
		uri = config.DBSourceTest
	}
	
	testDB, err = sql.Open(config.DBDriver, uri)
	if err != nil{
		log.Fatal("Cannot connect to db.", err)
	}

	testQueries = New(testDB)

	os.Exit(m.Run())
}