package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/josephhlwang/gobank/api"
	db "gitlab.com/josephhlwang/gobank/db/sqlc"
	"gitlab.com/josephhlwang/gobank/util"
)

func main(){
	config, err := util.LoadConfig(".")
	if err != nil{
		log.Fatal("Cannot load config.", err)
	}
	conn, err := sql.Open(config.DBDriver, config.DBSource)
	if err != nil{
		log.Fatal("Cannot connect to db.", err)
	}

	store := db.NewStore(conn)
	server := api.NewServer(store)

	err = server.Start(config.ServerAddress)
	if err != nil{
		log.Fatal("Cannot start server.", err)
	}
}